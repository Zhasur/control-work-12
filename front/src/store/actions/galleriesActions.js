import axios from '../../axios-api';
import {CREATE_GALLERY_SUCCESS, FETCH_GALLERIES_SUCCESS} from "./typeActions";
import {push} from 'connected-react-router';

const fetchGalleriesSuccess = galleries => ({type: FETCH_GALLERIES_SUCCESS, galleries});
const createGallerySuccess = () => ({type: CREATE_GALLERY_SUCCESS});

export const fetchGalleries = () => {
    return dispatch => {
        return axios.get('/galleries').then(response => {
            dispatch(fetchGalleriesSuccess(response.data))
        })
    }
};

export const createGallery = data => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const config = {headers: {"Authorization": token}};
        return axios.post('/galleries', data, config).then(
            response => {
                dispatch(createGallerySuccess());
                dispatch(push('/'))
            }
        )
    };
};
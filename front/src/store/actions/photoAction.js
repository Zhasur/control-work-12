import axios from '../../axios-api';
import {FETCH_PHOTO_SUCCESS} from "./typeActions";

const fetchPhotosSuccess = photos => ({type: FETCH_PHOTO_SUCCESS, photos});

export const fetchPhotos = galleryId => {
    return dispatch => {
        return axios.get('/photos?posts=' + galleryId).then(response => {
            dispatch(fetchPhotosSuccess(response.data));
        })
    };
};
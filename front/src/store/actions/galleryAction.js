import axios from '../../axios-api';
import {CREATE_PHOTO_SUCCESS, FETCH_GALLERY_SUCCESS} from "./typeActions";

const fetchGallerySuccess = gallery => ({type: FETCH_GALLERY_SUCCESS, gallery});
const createPhoto = () => ({type: CREATE_PHOTO_SUCCESS});

export const fetchGallery = galleryId => {
    return dispatch => {
        return axios.get('/galleries/' + galleryId).then(response => {
            dispatch(fetchGallerySuccess(response.data));
        })
    };
};

export const createPhotos = (photoData) => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const config = {headers: {"Authorization": token}};
        return axios.post('/photos', photoData, config).then(
            response => {
                dispatch(createPhoto());
            }
        )
    };
};
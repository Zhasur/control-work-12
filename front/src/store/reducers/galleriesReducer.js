import {FETCH_GALLERIES_SUCCESS} from "../actions/typeActions";

const initialState = {
    galleries: []
};

const galleriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_GALLERIES_SUCCESS:
            return {...state, galleries: action.galleries};
        default:
            return state;
    }
};

export default  galleriesReducer;
import {FETCH_PHOTO_SUCCESS} from "../actions/typeActions";

const initialState = {
    photos: []
};

const photoReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PHOTO_SUCCESS:
            return {...state, photos: action.photos};
        default:
            return state;
    }
};

export default  photoReducer;
import {FETCH_GALLERY_SUCCESS} from "../actions/typeActions";

const initialState = {
    gallery: []
};

const galleryReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_GALLERY_SUCCESS:
            return {...state, gallery: action.gallery};
        default:
            return state;
    }
};

export default  galleryReducer;
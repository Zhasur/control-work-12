import React, {Component} from 'react';
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props'
import {Button} from "reactstrap";
import {facebookLogin} from "../../store/actions/userAction";
import {connect} from "react-redux";
import {NotificationManager} from "react-notifications";

class FacebookLogin extends Component {
    facebookLogin = data => {
        console.log(data);
        if (data.error) {
            NotificationManager.error('Something went wrong');

        } else if (!data.name) {
            NotificationManager.warning('You canceled the process')
        } else {
            this.props.facebookLogin(data);
        }
    };
    render() {
        return (
            <FacebookLoginButton
                appId="2385783968119455"
                callback={this.facebookLogin}
                fields="name,email,picture"
                render={renderProps => (
                    <Button color="primary" onClick={renderProps.onClick}>
                        Login with Facebook
                    </Button>
                )}
            />
        );
    }
}

const mapDispatchToProps = dispatch => ({
    facebookLogin: userData => dispatch(facebookLogin(userData))
});

export default connect(null, mapDispatchToProps)(FacebookLogin);
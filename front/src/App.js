import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Route, Switch, withRouter} from "react-router";

import {connect} from "react-redux";
import {NotificationContainer} from "react-notifications";
import {logoutUser} from "./store/actions/userAction";

import Galleries from "./containers/Galleries/Galleries";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import Gallery from "./containers/Gallery/Gallery";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import NewGallery from "./containers/NewGallery/NewGallery";

class App extends Component {
    render() {
        return (
            <Fragment>
                <NotificationContainer/>
                <header>
                    <Toolbar user={this.props.user}
                             logout={this.props.logoutUser}/>
                </header>
                <Container>
                    <Switch>
                        <Route path="/" exact component={Galleries}/>
                        <Route path="/galleries/:id" exact component={Gallery}/>
                        <Route path="/register" exact component={Register}/>
                        <Route path="/login" exact component={Login}/>
                        <Route path="/new/gallery" exact component={NewGallery}/>
                    </Switch>
                </Container>
            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    user: state.users.user
});
const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));

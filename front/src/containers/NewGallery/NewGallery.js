import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {createGallery} from "../../store/actions/galleriesActions";
import {connect} from "react-redux";

class NewGallery extends Component {

    state = {
        title: '',
        image: '',
        user: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    submitFormHandler = event => {
        event.preventDefault();
        this.props.createGallery({...this.state});
    };
    render() {
        return (
            <Fragment>
                <div style={{margin: '30px 0'}}>
                    <h2>Create new gallery</h2>
                    <hr/>
                </div>
                <Form onSubmit={this.submitFormHandler}>
                    <FormGroup row>
                        <Label for="title" sm={2}>Title</Label>
                        <Col sm={10}>
                            <Input
                                value={this.state.title} onChange={this.inputChangeHandler}
                                type="text" name="title" id="title" />
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Label for="file" sm={2}>Image</Label>
                        <Col sm={10}>
                            <Input onChange={this.fileChangeHandler}
                                type="file" name="image" id="file" />
                        </Col>
                    </FormGroup>

                    <FormGroup check row>
                        <Col sm={{ size: 10, offset: 2 }}>
                            <Button type="submit" color="success">Create</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createGallery: data => dispatch(createGallery(data))
});

export default connect(null, mapDispatchToProps)(NewGallery);
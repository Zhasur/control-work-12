import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchGallery} from "../../store/actions/galleryAction";
import {fetchPhotos} from "../../store/actions/photoAction";
import {Col, Row} from "reactstrap";
import './Gallery.css';
import PostsThumbnail from "../../components/PostsThumblnile/PostsThumbnile";

class Gallery extends Component {

    componentDidMount() {
        this.props.fetchGallery(this.props.match.params.id);
        this.props.fetchPhotos(this.props.match.params.id);
    }

    render() {
        if (this.props.gallery) {
            console.log(this.props.gallery);
        }
        return (
            <Row>
                <Col sm={12}>
                    <h2 style={{textAlign: "center", marginTop: "30px"}}>
                        <strong>{this.props.gallery.title}</strong>
                    </h2>
                    <hr/>
                   <div className="PostBlock">
                       <div>
                           { this.props.gallery.image ?
                           <img className='PostImg'
                                src={'http://localhost:8050/uploads/' +
                                this.props.gallery.image}
                                alt={this.props.gallery.image}/>
                                : <PostsThumbnail/>
                           }
                       </div>
                   </div>
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    gallery: state.gallery.gallery,
    photos: state.photos.photos,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    fetchGallery: id => dispatch(fetchGallery(id)),
    fetchPhotos: id => dispatch(fetchPhotos(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Gallery);
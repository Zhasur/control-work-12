import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label, Row} from "reactstrap";
import {connect} from "react-redux";
import {createPhotos} from "../../store/actions/galleryAction";
import './AddPhoto.css';
import {fetchPhotos} from "../../store/actions/photoAction";

class AddPhoto extends Component {

    componentDidMount() {

    }

    state = {
        galleries: this.props.gallery,
        photos: '',
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    submitFormHandler = event => {
        event.preventDefault();
        this.props.createPhotos({...this.state});
        this.props.fetchPhotos(this.props.gallery)
    };

    render() {

        return (
            <Form onSubmit={this.submitFormHandler}>
                <div className="FormDiv">
                    <Row>
                        <Col sm={9}>
                            <FormGroup row>
                                <Label for="file" sm={2}>Image</Label>
                                <Col sm={10}>
                                    <Input onChange={this.fileChangeHandler}
                                           type="file" name="image" id="file" />
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col sm={3}>
                            <FormGroup check>
                                <Button type="submit" color="primary">
                                    Add comment
                                </Button>
                            </FormGroup>
                        </Col>
                    </Row>
                </div>
            </Form>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createPhotos: (photoData) => dispatch(createPhotos(photoData)),
    fetchPhotos: id => dispatch(fetchPhotos(id))
});

export default connect(null, mapDispatchToProps)(AddPhoto);
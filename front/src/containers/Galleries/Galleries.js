import React, {Component, Fragment} from 'react';
import {fetchGalleries} from "../../store/actions/galleriesActions";
import {connect} from "react-redux";
import './Galleries.css';
import {Link} from "react-router-dom";
import PostsThumbnail from "../../components/PostsThumblnile/PostsThumbnile";

class Galleries extends Component {

    componentDidMount() {
        this.props.fetchGalleries();
    }

    render() {
        const gallery = this.props.galleries.map(item => {
           return (
               <div className="Post" key={item._id}>
                   <div className='PostItem'>
                       <div>
                           {item.image ?
                           <img className='PostImg'
                                src={'http://localhost:8050/uploads/' + item.image}
                                alt={item.image}/>
                           : <PostsThumbnail/>}
                       </div>
                       <div className='PostTextBlock'>
                           <div>
                               <h2>{item.title}</h2>
                           </div>
                           <p> by <Link to={"/galleries/" + item.user._id}>{ item.user.username}</Link></p>
                       </div>
                   </div>
               </div>
           )
        });
        return (
            <Fragment>
                <div className="TitleText">
                    <h2 style={{textAlign: "center"}}><strong>Galleries</strong></h2>
                    <hr/>
                </div>
                <div className="main-block">
                    {gallery}
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    galleries: state.galleries.galleries
});

const mapDispatchToProps = dispatch => ({
    fetchGalleries: () => dispatch(fetchGalleries())
});

export default connect(mapStateToProps, mapDispatchToProps)(Galleries);
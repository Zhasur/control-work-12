const mongoose = require('mongoose');
const express = require('express');
const config = require('./config');
const cors = require('cors');

const users = require('./app/users');
const galleries = require('./app/galleries');
const photos = require('./app/photos');

const app = express();

app.use(express.json());
app.use(express.static('public'));
app.use(cors());

const port = 8050;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {

    app.use('/users', users);
    app.use('/galleries', galleries);
    app.use('/photos', photos);

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

});
const express = require('express');
const GallerySchema = require('../models/Galleries');
const auth = require('../middleware/auth');
const config = require('../config');
const multer = require('multer');
const nanoid = require('nanoid');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const gallery = await GallerySchema.find()
            .populate('user');
        return res.send(gallery);
    }catch (error) {
        return res.send(error)
    }
});

router.get('/:id', auth, async (req, res) => {
    try {
        const gallery = await GallerySchema.find({user: req.params._id}).populate('user');
        console.log(gallery);
        return res.send(gallery)
    }catch (error) {
        return res.send(error)
    }
});

router.post('/', [auth, upload.single('image')], (req, res) => {
    if (req.body.title || req.body.image){

        const galleryData = req.body;

        if (req.file) {
            galleryData.image = req.file.filename;
        }

        const gallery = new GallerySchema(galleryData);
        gallery.user = req.user._id;

        gallery.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error))
    }else {
        return res.status(400).send({error: "Title/Image is required"})
    }
});
module.exports = router;
const express = require('express');
const axios = require('axios');

const UserSchema = require('../models/User');
const auth = require('../middleware/auth');
const User = require('../models/User');

const router = express.Router();

router.post('/', (req, res) => {
    const user = new UserSchema(req.body);

    user.generateToken();

    user.save()
        .then(user => res.send({message: "User registered" , user}))
        .catch(error => res.status(400).send(error));
});

router.post('/sessions', async (req, res) => {
    const user = await UserSchema.findOne({username: req.body.username});
    if (!user) {
        return res.status(400).send({error: 'Username not found'});
    }

    const isMatch = await user.checkPassword(req.body.password);
    if (!isMatch) {
        return res.status(400).send({error: 'Password is wrong'})
    }

    user.generateToken();
    await user.save();

    return res.send({message: "Login success", user});
});

router.delete('/sessions', async (req, res) => {
    const token = req.get("Authorization");
    const success = {message: 'Logged out'};
    if (!token) {
        return res.send(success);
    }

    const user = await UserSchema.findOne({token});
    if (!user) {
        return res.send(success);
    }

    user.generateToken();
    await user.save();
    return res.send(success)
});

router.put('/', auth, async (req, res) => {
    req.users.password = req.body.password;
    await req.users.save();
    res.sendStatus(200);
});

router.post('/facebookLogin', async (req, res) => {
    const inputToken = req.body.accessToken;
    const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;
    const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

    try {
        const response = await axios.get(debugTokenUrl);

        const responseData = response.data;
        console.log(responseData);
        if (response.data.error) {
            return res.status(500).send({error: 'Token incorrect'});
        }

        if (responseData.data.user_id !== req.body.id) {
            return res.status(500).send({error: 'User is wrong!'});
        }

        let user = await User.findOne({facebookId: req.body.id});

        if (!user) {
            user = new User({
                username: req.body.email || req.body.id,
                password: nanoid(),
                facebookId: req.body.id
            })
        }

        user.generateToken();

        await user.save();

        return res.send({message: 'Login or register successful', user});

        return res.sendStatus(418);
    }  catch (e) {
        return res.status(500).send({error: 'Something went wrong'});
    }
});

module.exports = router;
const express = require('express');
const PhotoSchema = require('../models/Photos');
const auth = require('../middleware/auth');

const router = express.Router();

router.get('/:id', (req, res) => {
    if (req.query.galleries) {
        PhotoSchema.find({galleries: req.query.galleries}).populate('user')
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error))
    } else {
        PhotoSchema.find()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error))
    }

});

router.post('/', auth, (req, res) => {
    const photos = new PhotoSchema(req.body);
    photos.user = req.user._id;

    photos.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});

module.exports = router;
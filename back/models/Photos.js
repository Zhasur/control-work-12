const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const PhotoSchema = new Schema({
    galleries: {
        type: Schema.Types.ObjectId,
        ref: "Galleries",
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    photos: {
        type: String,
        required: true
    }
});

const Comment = mongoose.model('Photos', PhotoSchema);
module.exports = Comment;
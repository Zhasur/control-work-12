const mongoose = require('mongoose');
const config = require('./config');

const Gallery = require('./models/Galleries');
const User = require('./models/User');
const Photos = require('./models/Photos');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }


    const user = await User.create(
        {username: 'Modern_man', password: '123', token: 1},
        {username: 'Designer', password: '123', token: 2},
        {username: 'Art_lover', password: '123', token: 3},
        {username: 'Nature_keeper', password: '123', token: 4},
    );

    const gallery = await Gallery.create(
        {
            title: "The Modernism",
            user: user[0]._id,
            image: 'modernism.jpg'
        },
        {
            title: "Art Gallery",
            user: user[2]._id,
            image: 'art_gallery.jpg'
        },
        {
            title: "Design Gallery",
            user: user[1]._id,
            image: 'design_gallery.jpg'
        },
        {
            title: "Nature",
            user: user[1]._id,
            image: 'nature.jpg'
        }
    );

    await Photos.create(
        {galleries: gallery[0]._id, user: user[0]._id, photos: "modernism.jpg"},
        {galleries: gallery[2]._id, user: user[2]._id, photos: "art_gallery.jpg"},
        {galleries: gallery[1]._id, user: user[1]._id, photos: "design_gallery.jpg"},
        {galleries: gallery[3]._id, user: user[3]._id, photos: "nature.jpg"}
    );
    await connection.close();
};

run().catch(error => {
    console.error('Something went wrong', error)
});